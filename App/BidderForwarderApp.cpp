//

#include "GUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <memory>
#include <string>

#include <boost/thread.hpp>
#include <thread>

#include "BidderForwarderAppStarter.h"
int main(int argc, char* argv[]) {
        auto bidderAppStarter = std::make_shared<BidderForwarderAppStarter>();

        bidderAppStarter->startTheApp(argc, argv);
}
