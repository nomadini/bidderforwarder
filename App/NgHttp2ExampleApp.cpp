// //
//
// #include "GUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <memory>
// #include <string>
//
// #include <boost/thread.hpp>
// #include <thread>
// #include <nghttp2/asio_http2_server.h>
// #include <nghttp2/asio_http2_client.h>
// #include "Poco/Util/ServerApplication.h"
// #include "ForwarderModuleContainer.h"
// #include "BidderForwarderRequestHandlerFactory.h"
// #include "BidderForwarder.h"
// #include "BeanFactory.h"
//
//
// using boost::asio::ip::tcp;
//
// std::multimap<std::string, nghttp2::asio_http2::header_value>
// createHeaderMapWithHeader(std::string headerName, std::string headerValueRaw) {
//         std::multimap<std::string, nghttp2::asio_http2::header_value>
//         headerMap;
//         nghttp2::asio_http2::header_value headerValue;
//         headerValue.value = headerValueRaw;
//         headerValue.sensitive = false;
//         headerMap.insert(std::make_pair( headerName, headerValue));
//         return headerMap;
//
// }
//
// nghttp2::asio_http2::header_value createHeaderValue(std::string headerValueRaw) {
//
//         nghttp2::asio_http2::header_value headerValue;
//
//         headerValue.value = headerValueRaw;
//         headerValue.sensitive = false;
//
//         return headerValue;
// }
//
// void processResponse(const uint8_t *data, std::size_t len) {
//         std::cerr << "data from server : ";
//         std::cerr.write(reinterpret_cast<const char *>(data), len);
//         std::cerr << std::endl;
// }
//
// void sendRequest(nghttp2::asio_http2::client::session& sess,
//                  std::string url,
//                  std::string methodType,
//                  std::string requestBody,
//                  std::multimap<std::string, nghttp2::asio_http2::header_value> headerMap) {
//         using namespace nghttp2::asio_http2;
//         using namespace nghttp2::asio_http2::client;
//
//         boost::system::error_code ec;
//         auto req = sess.submit(ec, methodType, url, requestBody, headerMap);
//
//         req->on_response([](const nghttp2::asio_http2::client::response &res) {
//                 // print status code and response header fields.
//                 std::cerr << "HTTP/2 " << res.status_code() << std::endl;
//                 for (auto &kv : res.header()) {
//                         std::cerr << "header : " << kv.first << ": " << kv.second.value << "\n";
//                 }
//                 std::cerr << std::endl;
//
//                 res.on_data([](const uint8_t *data, std::size_t len) {
//                         processResponse(data, len);
//
//                 });
//         });
//
//         req->on_close([&sess](uint32_t error_code) {
//                 // shutdown session after first request was done.
//                 // sess.shutdown();
//         });
// }
// void setupClient(std::string host, int port) {
//         using namespace nghttp2::asio_http2;
//         using namespace nghttp2::asio_http2::client;
//         boost::system::error_code ec;
//         boost::asio::io_service io_service;
//
//         session sess(io_service, host.c_str(), _toStr(port));
//         sess.on_connect([&sess](tcp::resolver::iterator endpoint_it) {
//
//                 auto
//                 headerMap =
//                         createHeaderMapWithHeader("CookieSetByClient", "a=1; b=3");
//
//                 headerMap.insert(std::make_pair( "CookieSetByClient2", createHeaderValue("hassan")));
//                 headerMap.insert(std::make_pair( "CookieSetByClient3", createHeaderValue("reza")));
//
//                 for (int i =0; i< 3; i++) {
//                         // sendRequest(sess, "http://localhost:3000/",
//                         //             "GET",
//                         //             "anyBody in there?",
//                         //             headerMap);
//                         sendRequest(sess, "http://localhost:3000/abc",
//                                     "POST",
//                                     "anyBody?",
//                                     headerMap);
//                 }
//         });
//
//         sess.on_error([](const boost::system::error_code &ec) {
//                 std::cerr << "error: " << ec.message() << std::endl;
//         });
//
//         io_service.run();
//
// }
//
// void registerHandler(nghttp2::asio_http2::server::http2& server,
//                      std::string path,
//                      nghttp2::asio_http2::server::request_cb requestCallBack) {
//         server.handle(path, requestCallBack);
// }
//
// int numberOfRequests = 0;
//
// std::unordered_map<std::string, std::string>
// getHeadersAsMap(std::multimap<std::string, nghttp2::asio_http2::header_value> headers) {
//         std::unordered_map<std::string, std::string> simpleHeaders;
//         for ( auto pair : headers) {
//                 simpleHeaders.insert(std::make_pair(pair.first, pair.second.value));
//         }
//         return simpleHeaders;
// }
//
// std::string getHeadersAsJson(std::multimap<std::string, nghttp2::asio_http2::header_value> headers) {
//         std::unordered_map<std::string, std::string> simpleHeaders;
//         for ( auto pair : headers) {
//                 simpleHeaders.insert(std::make_pair(pair.first, pair.second.value));
//         }
//         return JsonMapUtil::convertMapToJsonArray(simpleHeaders);
// }
//
// void abcReqHanlder(const nghttp2::asio_http2::server::request &req,
//                    const nghttp2::asio_http2::server::response & res) {
//
//
//         std::multimap<std::string, nghttp2::asio_http2::header_value> clientSetHeaders =  req.header();
//
//         // Returns method (e.g., GET).
//         std::string requestMethod = req.method();;
//
//         // Returns request URI, split into components.
//         nghttp2::asio_http2::uri_ref requestUri =  req.uri();
//
//         LOG(INFO)
//                 << "headersSentByClient " << getHeadersAsJson(clientSetHeaders) << std::endl
//                 << "requestMethod " << requestMethod << std::endl
//                 << "scheme " << requestUri.scheme << std::endl
//                 << "host " << requestUri.host << std::endl
//                 << "path " << requestUri.path << std::endl
//                 << "raw_path " << requestUri.raw_path << std::endl
//                 << "raw_query " << requestUri.raw_query << std::endl
//                 << "fragment " << requestUri.fragment << std::endl;
//
//         req.on_data([](const uint8_t *data, std::size_t len) {
//                 std::string requestBody(reinterpret_cast<const char *>(data), len);
//
//                 std::cerr << "data from client with length : "
//                           <<len
//                           <<": "<< requestBody<< std::endl;
//
//                 // std::cerr << "data from client : ";
//                 // std::cerr.write(reinterpret_cast<const char *>(data), len);
//                 // std::cerr << std::endl;
//
//         });
//         auto
//         headerMap =
//                 createHeaderMapWithHeader("Cookie", "a=1; b=3");
//
//         headerMap.insert(std::make_pair( "Cookie1", createHeaderValue("hassan")));
//         headerMap.insert(std::make_pair( "Cookie2", createHeaderValue("reza")));
//
//         res.write_head(200, headerMap);
//         numberOfRequests++;
//
//         std::string response = "abcReqHanlder talking here : " + _toStr(numberOfRequests) + "\n";
//         res.end(response);
// }
//
// void startServer(std::string host, int port) {
//
//         nghttp2::asio_http2::server::http2 server;
//
//         server.num_threads(10);
//         server.backlog(1000);
//
//         // registerHandler(server, "/", &helloWorldReqHanlder);
//         registerHandler(server, "/abc", &abcReqHanlder);
//
//         boost::system::error_code ec;
//
//         if (server.listen_and_serve(ec, host.c_str(), _toStr(port))) {
//                 std::cerr << "error: " << ec.message() << std::endl;
//         }
//         std::cout<<"start real server\n";
// }
//
//
// #include "ObjectGlobalMapContainer.h"
//
// void startNomadiniClient() {
//         // NomadiniNghttp2Client client(10, "localhost", 3000);
//         // gicapods::Util::sleepMiliSecond(3000);
//         // auto
//         // headerMap =
//         //         NomadiniNgHttp2Util::createHeaderMapWithHeader("CookieSetByClient", "a=1; b=3");
//         // std::string url = "http://localhost:3000/abc";
//         // std::string methodType = "POST";
//         // std::string requestBody =
//         //         "{\"name\" : \"hassanRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminhassanRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminhassanRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminRezaAminMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjadMohammadAliHassanHosseinSajjad\"}";
//         // int requestTimeoutInMillis = 100;
//         // int responseStatusCode = 200;
//         // std::string responseBody;
//         // std::unordered_map<std::string, std::string> requestHeaders;
//         // std::unordered_map<std::string, std::string> responseHeaders;
//         //
//         // for (int i =0; i< 100; i++) {
//         //
//         //         // sendRequest(sess, "http://localhost:3000/",
//         //         //             "GET",
//         //         //             "anyBody in there?",
//         //         //             headerMap);
//         //         client.sendRequest(url,
//         //                            methodType,
//         //                            requestBody,
//         //                            responseStatusCode,
//         //                            responseBody,
//         //                            requestHeaders,
//         //                            responseHeaders);
//         //
//         //         // std::cout<<"response by server : "<<
//         //         //         client.getResponse(responseKeyByServer, 2, 20000)<<std::endl;
//         //
//         // }
// }
// int main2(int argc, char* argv[]) {
//
//         std::thread startClientThread(&startNomadiniClient);
//         startClientThread.detach();
//
//         startServer("localhost", 3000);
//
//         std::cout<<"end of program\n";
// }
//
// int mainDisabled(int argc, char* argv[]) {
//
//
//         std::thread clientThread (&setupClient, "localhost", 3000);
//         clientThread.detach();
//
//
//
//         startServer("localhost", 3000);
//
// }
