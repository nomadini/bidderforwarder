#include "GUtil.h"
#include "MySqlMetricService.h"
#include "MySqlDriver.h"
#include "ForwardContextBuilderModule.h"
#include "OpenRtbBidRequestParser.h"
#include "ForwarderModuleContainer.h"

ForwarderModuleContainer::ForwarderModuleContainer() {

}

ForwarderModuleContainer::~ForwarderModuleContainer() {

}

void ForwarderModuleContainer::initializeModules() {

        assertAndThrow(!propertyFileName.empty());
        assertAndThrow(!commonPropertyFileName.empty());
        LOG(INFO) << "propertyFileName : "<< propertyFileName;
        LOG(INFO) << "commonPropertyFileName : "<< commonPropertyFileName;

        configService = std::make_unique<gicapods::ConfigService>(propertyFileName,
                                                                  commonPropertyFileName);

        entityToModuleStateStats = EntityToModuleStateStats::getInstance(configService.get());

        LogLevelManager::shared_instance()->allLogsAreOn = configService->getAsBooleanFromString("allLogsAreOn");
        LogLevelManager::shared_instance()->allLoggingIsTurendOff = configService->getAsBooleanFromString("allLoggingIsTurendOff");

        auto openRtbBidRequestParser = std::make_shared<OpenRtb2_3_0::OpenRtbBidRequestParser>();
        openRtbBidRequestParser->entityToModuleStateStats = entityToModuleStateStats.get();

        forwardContextBuilderModule = std::make_unique<ForwardContextBuilderModule> (
                configService.get(),
                entityToModuleStateStats.get());
        forwardContextBuilderModule->openRtbBidRequestParser = openRtbBidRequestParser;

        mySqlDriverMango = MySqlDriver::getInstance(configService.get());
        mySqlMetricService = std::make_unique<MySqlMetricService>(mySqlDriverMango.get());
        entityToModuleStateStatsPersistenceService =
                std::make_unique<EntityToModuleStateStatsPersistenceService>(
                        entityToModuleStateStats.get(),
                        mySqlMetricService.get(),
                        configService.get(),
                        appName,
                        appVersion
                        );

}
