
#include "Status.h"
#include "AtomicLong.h"
#include "StringUtil.h"
#include "BidRequestForwarderPipelineProcessor.h"
#include "BidderForwarderModule.h"
#include "EntityToModuleStateStats.h"
#include "ForwarderContext.h"
#include <memory>
#include <string>
#include "BidderForwarderModule.h"
#include "EntityToModuleStateStats.h"
#include "ConfigService.h"
#include "ForwarderModuleContainer.h"
#include "EntityToModuleStateStats.h"

BidRequestForwarderPipelineProcessor::BidRequestForwarderPipelineProcessor()  {

}

void BidRequestForwarderPipelineProcessor::setTheModuleList() {

}

void BidRequestForwarderPipelineProcessor::addToPreBidModuleList(BidderForwarderModule* module) {
        NULL_CHECK(module);
        this->preBidModules.push_back (module);
}

void BidRequestForwarderPipelineProcessor::runModule(
        BidderForwarderModule* module, std::shared_ptr<ForwarderContext> context) {
        Poco::Timestamp moduleStartTime;
        module->beforeProcessing(context);
        module->process (context);
        module->afterProcessing(context);
}

void BidRequestForwarderPipelineProcessor::process(std::shared_ptr<ForwarderContext> opt) {
        entityToModuleStateStats->addStateModuleForEntity("#OfProcessingBidRequests", "BidRequestForwarderPipelineProcessor", "ALL");


        for (std::vector<BidderForwarderModule*>::iterator it = preBidModules.begin ();
             it != preBidModules.end (); ++it) {
                runModule(*it, opt);


                if (opt->status->value == STOP_PROCESSING) {
                        opt->lastModuleRunInPipeline = (*it)->getName ();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "LastModule-" + (*it)->getName (),
                                "BidRequestForwarderPipelineProcessor",
                                "ALL");
                        break;
                }
        }



}

std::string BidRequestForwarderPipelineProcessor::getName() {
        return "BidRequestForwarderPipelineProcessor";
}

BidRequestForwarderPipelineProcessor::~BidRequestForwarderPipelineProcessor() {

}
