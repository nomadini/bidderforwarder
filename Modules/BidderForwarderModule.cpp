#include "BidderForwarderModule.h"

#include "EntityToModuleStateStats.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "Poco/DateTimeParser.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/LocalDateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "ForwarderContext.h"
BidderForwarderModule::BidderForwarderModule(EntityToModuleStateStats* entityToModuleStateStats)
        : Object(__FILE__) {

        assertAndThrow(entityToModuleStateStats != NULL);
        this->entityToModuleStateStats = entityToModuleStateStats;
}

BidderForwarderModule::~BidderForwarderModule() {
}

void BidderForwarderModule::beforeProcessing(std::shared_ptr<ForwarderContext> context) {

}

std::string BidderForwarderModule::getName() {
        return "BidderForwarderModule_X";
}

void BidderForwarderModule::afterProcessing(std::shared_ptr<ForwarderContext> context) {

}
