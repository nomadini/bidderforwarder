#ifndef ForwarderModuleContainer_H
#define ForwarderModuleContainer_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "ConfigService.h"
#include "Object.h"



class ForwardContextBuilderModule;
#include <unordered_set>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_queue.h>
#include "AtomicDouble.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "EntityToModuleStateStats.h"
#include "AtomicBoolean.h"

#include "MySqlDriver.h"
#include "ConfigService.h"

class ForwarderModuleContainer {

public:
std::string propertyFileName;
std::string appName;
std::string appVersion;
std::string commonPropertyFileName;

std::shared_ptr<MySqlDriver> mySqlDriverMango;
std::shared_ptr<EntityToModuleStateStatsPersistenceService> entityToModuleStateStatsPersistenceService;
std::shared_ptr<EntityToModuleStateStats> entityToModuleStateStats;
std::unique_ptr<gicapods::ConfigService> configService;
std::unique_ptr<MySqlMetricService> mySqlMetricService;
std::unique_ptr<ForwardContextBuilderModule> forwardContextBuilderModule;

ForwarderModuleContainer();

void addFilters();
void initializeModules();
virtual ~ForwarderModuleContainer();

};

#endif
