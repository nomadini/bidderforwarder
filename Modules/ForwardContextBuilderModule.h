/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef ForwardContextBuilderModule_H_
#define ForwardContextBuilderModule_H_

#include "Status.h"

class ForwarderContext;
class EntityToModuleStateStats;
class HttpUtilService;
class PocoSession;
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include "BidderForwarderModule.h"
#include "ConcurrentHashSet.h"
#include "ConfigService.h"
#include "Poco/Net/HTTPClientSession.h"
#include "OpenRtbBidRequestParser.h"
class ForwardContextBuilderModule : public BidderForwarderModule, public Object {

private:

public:

virtual std::string getName();
std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequestParser> openRtbBidRequestParser;

ForwardContextBuilderModule(
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats);

virtual void process(std::shared_ptr<ForwarderContext> context);

};

#endif
