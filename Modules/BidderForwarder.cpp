#include "GUtil.h"

#include "BidderForwarderRequestHandlerFactory.h"
#include "BidderForwarder.h"
#include "ConfigService.h"
#include "PocoHttpServer.h"
#include "OpenRtbVersion.h"
BidderForwarder::BidderForwarder() {
}

BidderForwarder::~BidderForwarder() {

}

std::string BidderForwarder::getName() {
        return "BidderForwarder";
}

void BidderForwarder::init() {
        httpServer = PocoHttpServer::createHttpServer(configService, requestHandlerFactory);
}

/**
   No one is calling this now. but this is another incomplete implemnetation
   that might have greate performance in future
 */
void BidderForwarder::buildNgHttp2Server() {
        // httpServer = std::make_shared<NomadiniNghttp2Server>(
        //         "localhost",
        //         configService->getAsInt("httpPort"),
        //         10,
        //         100);
        // NgHttp2BidRequestForwarder* ngHttp2BidRequestForwarder = requestHandlerFactory->createNgHttp2RequestHandler(
        //         "/bid/google/openrtb2.3",
        //         OpenRtbVersion::V2_3
        //         );
        // std::function<void(
        //                       const nghttp2::asio_http2::server::request &request,
        //                       const nghttp2::asio_http2::server::response & response
        //                       )> callback;
        // callback =
        //         std::bind(&NgHttp2BidRequestForwarder::handleRequest,
        //                   ngHttp2BidRequestForwarder,
        //                   std::placeholders::_1, std::placeholders::_2);
        // httpServer->registerHandler(
        //         "/bid/google/openrtb2.3",
        //         callback
        // );
}
int BidderForwarder::main(const std::vector<std::string> &args) {

        // start the HTTPServer
        httpServer->start ();
        // wait for CTRL-C or kill

        waitForTerminationRequest ();
        // Stop the HTTPServer
        httpServer->stop ();
        LOG(ERROR)<<"server is shutting down......";
        return Application::EXIT_OK;
}
