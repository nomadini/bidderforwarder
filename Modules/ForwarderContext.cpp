#include "GUtil.h"
#include "Campaign.h"
#include "StringUtil.h"
#include "ForwarderContext.h"
#include "JsonUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

#include "Device.h"
ForwarderContext::ForwarderContext() : Object(__FILE__) {
        requestIsParsedOnlyForForwarding = true;
        status = std::make_shared<gicapods::Status> ();
}

ForwarderContext::~ForwarderContext() {
}
