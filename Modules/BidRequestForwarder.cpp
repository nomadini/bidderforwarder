
#include "BidRequestForwarder.h"
#include <boost/exception/all.hpp>
#include "StringUtil.h"
#include "HttpUtil.h"
#include "BidRequestForwarderPipelineProcessor.h"
#include "ForwarderContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonMapUtil.h"
#include "Poco/Timestamp.h"

BidRequestForwarder::BidRequestForwarder(
        std::shared_ptr<ForwarderContext> context,
        BidRequestForwarderPipelineProcessor* bidRequestForwarderPipelineProcessor,
        EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->context = context;
        this->bidRequestForwarderPipelineProcessor = bidRequestForwarderPipelineProcessor;
}


void BidRequestForwarder::handleRequest(Poco::Net::HTTPServerRequest &request,
                                        Poco::Net::HTTPServerResponse &response) {

        Poco::Timestamp timeToProcessRequest;

        try {
                numberOfBidRequestRecievedPerMinute->increment();
                entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsRecieved",
                                                                   "BidRequestForwarder",
                                                                   "ALL");
                context->requestIsParsedOnlyForForwarding = true;
                context->request = &request;
                context->requestBody = HttpUtil::getRequestBody (request);
                MLOG(3) << "context->requestBody : "<<context->requestBody;
                //parse request and get id
                //pick a bidder based on id
                //call a bidder with reques
                bidRequestForwarderPipelineProcessor->process(context);
                if(context->finalBidResponseContent.empty()) {
                        context->bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_NO_CONTENT;

                        entityToModuleStateStats->addStateModuleForEntity ("EMPTY_BID_RESPONSE_AKA_NO_BID",
                                                                           "BidRequestForwarder",
                                                                           "ALL");
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("NON_EMPTY_BID_RESPONSE_AKA_BID",
                                                                           "BidRequestForwarder",
                                                                           "ALL");
                        context->bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_OK;
                        LOG_EVERY_N(ERROR, 1000) << "sent " << google::COUNTER << "th good bid";
                }

                assertAndThrow(context->bidResponseStatusCode ==  Poco::Net::HTTPResponse::HTTP_OK
                               || context->bidResponseStatusCode == Poco::Net::HTTPResponse::HTTP_NO_CONTENT);
                response.setStatus (context->bidResponseStatusCode);

                std::unordered_map<std::string,std::string > headers;

                HttpUtil::getResponseHeaders(headers, response);
                auto headersJson = JsonMapUtil::convertMapToJsonArray(headers);
                MLOG(3) << "headers : "<< headersJson;
                MLOG(3) << "getKeepAlive() : "<< response.getKeepAlive();

                //works with normal mode
                std::ostream &ostr = response.send ();
                ostr << context->finalBidResponseContent;

                LOG_EVERY_N(ERROR, 1000) << "sent " << google::COUNTER << "th bid";

        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity ("Known_Exception"+ _toStr(e.what()),
                                                                   "BidRequestForwarder",
                                                                   "ALL");
        }

        catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "BidRequestForwarder",
                                                                   "ALL");

        }

        HttpUtil::setEmptyResponse(response);
        LOG_EVERY_N(INFO, 1000) << google::COUNTER<<"nth response took " << timeToProcessRequest.elapsed () / 1000 <<" milis ";
}
