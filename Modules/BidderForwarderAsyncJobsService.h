#ifndef bidderAsyncJobsService_H_
#define bidderAsyncJobsService_H_

class GUtil;

#include <memory>
#include <string>
#include <boost/thread.hpp>
#include <thread>
#include "DateTimeMacro.h"
#include "Poco/Net/HTTPServer.h"
class EntityToModuleStateStats;
class MySqlTargetGroupBWListMapService;
class EntityToModuleStateStatsPersistenceService;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class BidderRequestHandlerFactory;
class MySqlTargetGroupFilterCountDbRecordService;
class OriginalBidderCallerModule;

#include "DataClient.h"
#include "TargetGroupFilterStatistic.h"

#include "PocoHttpServer.h";
class FeatureDeviceHistory;
class PocoHttpServer;
class BidderForwarderAsyncJobsService;

namespace gicapods {
class AtomicLong;
class ConfigService;
template <typename K, typename V>
class ConcurrentHashMap;
}
class BidderForwarderAsyncJobsService : public std::enable_shared_from_this<BidderForwarderAsyncJobsService> {

public:
std::shared_ptr<gicapods::AtomicLong> numberOfBidRequestRecievedPerMinute;
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
OriginalBidderCallerModule* originalBidderCallerModule;

std::shared_ptr<Poco::Net::HTTPServer> httpServer;
BidderForwarderAsyncJobsService();

virtual ~BidderForwarderAsyncJobsService();

void runEveryMinute();

void runEvery10Seconds();
void runEveryHour();

std::string getName();

void startBidderIfDead();
void startAsyncThreads();
void printServerInfo();
static void printMemoryUsage(double& vm_usage, double& resident_set);

private:
bool _helpRequested;
};

#endif
