#ifndef BidRequestForwarder_H
#define BidRequestForwarder_H

#include "SignalHandler.h"
#include <memory>
#include <string>
class ForwarderContext;
class TargetGroup;
class EntityToModuleStateStats;
class BidResponseBuilder;
class TargetGroupCacheService;
class BidModeControllerService;
class BidRequestForwarderPipelineProcessor;
#include "AtomicDouble.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "AtomicLong.h"
#include "ConcurrentHashMap.h"
#include "StringHolder.h"

class BidRequestForwarder : public Poco::Net::HTTPRequestHandler {

public:

std::shared_ptr<gicapods::AtomicLong> numberOfBidRequestRecievedPerMinute;

std::shared_ptr<ForwarderContext> context;
EntityToModuleStateStats* entityToModuleStateStats;
BidRequestForwarderPipelineProcessor* bidRequestForwarderPipelineProcessor;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, StringHolder> > transactionIdToResponse;
BidRequestForwarder(
        std::shared_ptr<ForwarderContext> forwarderContext,
        BidRequestForwarderPipelineProcessor* bidRequestForwarderPipelineProcessor,
        EntityToModuleStateStats* entityToModuleStateStats);

void handleRequest(Poco::Net::HTTPServerRequest &request,
                   Poco::Net::HTTPServerResponse &response);

};

#endif
