#include "Status.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "ForwarderContext.h"

#include "EntityToModuleStateStats.h"
#include "OriginalBidderCallerModule.h"
#include "ConfigService.h"
#include "JsonUtil.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "ExceptionUtil.h"
#include "EntityToModuleStateStats.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Timestamp.h"
#include "Device.h"
#include "PocoSession.h"
#include "ConcurrentHashMap.h"

OriginalBidderCallerModule::OriginalBidderCallerModule(
        std::vector<std::string> allBidderHosts,
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats)
        : BidderForwarderModule(entityToModuleStateStats), Object(__FILE__) {
        indexToBidderHttpSessionMap = std::make_shared<gicapods::ConcurrentHashMap<int, PocoSession > >();
        this->allBidderHosts = allBidderHosts;

        for (int i = 0; i < allBidderHosts.size(); i++) {
                auto session = std::make_shared<PocoSession>(
                        "OriginalBidderCallerModule",
                        configService->getAsBooleanFromString("bidderCallKeepAliveMode"),
                        configService->getAsInt("biderKeepAliveTimeoutInMillis"),
                        configService->getAsInt("bidderCallTimeoutInMillis"),
                        allBidderHosts.at(i),
                        entityToModuleStateStats);

                indexToBidderHttpSessionMap->put(i, session);
        }


        methodType = "POST";

}


std::string OriginalBidderCallerModule::getName() {
        return "OriginalBidderCallerModule";
}


int OriginalBidderCallerModule::getSessionBasedOnUserId(std::shared_ptr<ForwarderContext> context) {
        int index = StringUtil::getHashedNumber (context->device->getDeviceId()) % allBidderHosts.size();
        return index;
}

void OriginalBidderCallerModule::process(std::shared_ptr<ForwarderContext> context) {


        std::string responseFromBidder;

        if (context->device == nullptr) {
                entityToModuleStateStats->addStateModuleForEntity ("NOT_FORWARDED_REASON:UNKNOWN_DEVICE",
                                                                   "OriginalBidderCallerModule",
                                                                   "ALL");
                return;
        }


        int sessionNumber = getSessionBasedOnUserId(context);
        auto pocoSession = indexToBidderHttpSessionMap->get(sessionNumber);
        try {
                assertAndThrow(!context->servletPathToForwardTo.empty());
                context->processingBidderUrl = allBidderHosts.at(sessionNumber) + context->servletPathToForwardTo;
                // if (remoteBidderInfo->numberOfFailures->getValue() > 300) {
                //         entityToModuleStateStats->addStateModuleForEntity ("THROTTING_BIDDER_: " + context->processingBidderUrl,
                //                                                            "OriginalBidderCallerModule",
                //                                                            "ALL");
                //         LOG_EVERY_N(ERROR, 1000)<< google::COUNTER<<"th throttling bidder requests";
                //         return;
                // }

                auto response = pocoSession->sendRequest(
                        *PocoHttpRequest::createSimplePostRequest(context->processingBidderUrl,
                                                                  context->requestBody));



                LOG_EVERY_N(INFO, 1000) << google::COUNTER<<"th response from bidder";
                entityToModuleStateStats->addStateModuleForEntity ("FORWARDED_TO_: " + context->processingBidderUrl,
                                                                   "OriginalBidderCallerModule",
                                                                   "ALL");
                context->finalBidResponseContent = response->body;
        } catch (std::exception const &e) {
                ExceptionUtil::logException(e, entityToModuleStateStats, 1000);
                // remoteBidderInfo->numberOfFailures->increment();
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_TO_FORWARD_TO"+
                                                                   context->processingBidderUrl
                                                                   + "_REASON_"
                                                                   +_toStr(e.what()),
                                                                   "OriginalBidderCallerModule",
                                                                   "ALL");
        }



}


void OriginalBidderCallerModule::resetBidderFailures() {
        auto map = *indexToBidderHttpSessionMap->getCopyOfMap();
        for (auto iter = map.begin (); iter != map.end (); iter++) {
                // if ((*iter).second->numberOfFailures->getValue() > 0) {
                //         (*iter).second->numberOfFailures->setValue(0);
                // }
        }
}
