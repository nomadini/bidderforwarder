/*
 * BidderForwarderAppStarter.h
 *
 *  Created on: Aug 26, 2015
 *      Author: mtaabodi
 */

#ifndef BidderForwarderAppStarter_H_
#define BidderForwarderAppStarterH_
#include <memory>
#include <string>
class BidderForwarderAppStarter;



class BidderForwarderAppStarter {

public:
void startTheApp(int argc, char* argv[]);
void processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory);
};

#endif
