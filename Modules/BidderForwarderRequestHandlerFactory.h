#ifndef BidderForwarderRequestHandlerFactory_H
#define BidderForwarderRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class ForwarderContext;
class BidRequestForwarder;

class BidRequestForwarderPipelineProcessor;
class EntityToModuleStateStats;
namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "AtomicDouble.h"
#include "OpenRtbVersion.h"
class NgHttp2BidRequestForwarder;
#include "Poco/Logger.h"
class BidderForwarderRequestHandlerFactory;



class BidderForwarderRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

public:

std::shared_ptr<gicapods::AtomicLong> numberOfBidRequestRecievedPerMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfRequestsProcessingNow;
std::shared_ptr<Poco::Timestamp> timeOfProcessingBatch;
BidRequestForwarderPipelineProcessor* bidRequestForwarderPipelineProcessor;
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
BidderForwarderRequestHandlerFactory(BidRequestForwarderPipelineProcessor* bidRequestForwarderPipelineProcessor,
                                     EntityToModuleStateStats* entityToModuleStateStats,
                                     gicapods::ConfigService* configService);

Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);

BidRequestForwarder* createBidRequestForwarder(
        std::string uriPath,
        OpenRtbVersion openRtbVersion);

// NgHttp2BidRequestForwarder* createNgHttp2RequestHandler(std::string uriPath,
//                                                         OpenRtbVersion openRtbVersion);
};

#endif
