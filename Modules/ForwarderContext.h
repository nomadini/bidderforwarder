#ifndef ForwarderContext_H
#define ForwarderContext_H

class StringUtil;
#include <memory>
#include <string>
#include "Poco/Net/NameValueCollection.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPResponse.h"
#include "tbb/concurrent_hash_map.h"
#include "OpenRtbVersion.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "AtomicLong.h"
#include "DateTimeMacro.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
#include "IntWrapper.h"
#include <tbb/concurrent_unordered_set.h>
#include <unordered_set>
#include "Status.h"

class ForwarderContext : public Object {

private:

public:
ForwarderContext();
virtual ~ForwarderContext();
std::shared_ptr<gicapods::Status> status;

Poco::Net::HTTPServerRequest* request;
std::string exchangeName;
std::string servletPathToForwardTo;
std::string processingBidderUrl;
std::shared_ptr<Device> device;
OpenRtbVersion openRtbVersion;
bool requestIsParsedOnlyForForwarding;
std::string lastModuleRunInPipeline;
std::string requestBody;
std::string finalBidResponseContent;
Poco::Net::HTTPResponse::HTTPStatus bidResponseStatusCode;
};

#endif
