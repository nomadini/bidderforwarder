#ifndef BidderForwarder_H_
#define BidderForwarder_H_


#include <memory>
#include <string>
#include "Poco/Util/ServerApplication.h"

#include "Poco/Net/HTTPServer.h"
class BidderForwarderApplicationContext;
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }
class BidderForwarderRequestHandlerFactory;
class BidderForwarderAsyncJobsService;

class BidderForwarder;

#include "PocoHttpServer.h";



class BidderForwarder : public std::enable_shared_from_this<BidderForwarder>
        , public Poco::Util::ServerApplication
{

public:
std::shared_ptr<Poco::Net::HTTPServer> httpServer;
gicapods::ConfigService* configService;
BidderForwarderRequestHandlerFactory* requestHandlerFactory;
EntityToModuleStateStats* entityToModuleStateStats;
std::string propertyFileName;

BidderForwarder();
virtual ~BidderForwarder();
void buildNgHttp2Server();
std::string getName();
void init();

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
