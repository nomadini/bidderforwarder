#include "GUtil.h"
#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "BidderForwarderAsyncJobsService.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include "StringUtil.h"
#include <stdlib.h> //for system
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include "EntityToModuleStateStats.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "DataReloadService.h"
#include "PocoHttpServer.h"
#include "PocoHttpServer.h"
#include "ConfigService.h"
#include "FileUtil.h"
#include <boost/thread.hpp>
#include <thread>
#include "AtomicLong.h"
#include "DataClient.h"
#include "ConcurrentHashMap.h"
#include "DateTimeUtil.h"
#include "OriginalBidderCallerModule.h"

BidderForwarderAsyncJobsService::BidderForwarderAsyncJobsService() {

}

BidderForwarderAsyncJobsService::~BidderForwarderAsyncJobsService() {

}

void BidderForwarderAsyncJobsService::printServerInfo() {
        LOG_EVERY_N(INFO, 1) <<
                "currentConnections : " <<httpServer->currentConnections()<<
                ", currentThreads : " <<httpServer->currentThreads()<<
                ",maxConcurrentConnections : " <<httpServer->maxConcurrentConnections()<<
                ",maxThreads : " <<httpServer->maxThreads()<<
                ",queuedConnections : " <<httpServer->queuedConnections()<<
                ",refusedConnections : " <<httpServer->refusedConnections()<<
                ", totalConnections : " <<httpServer->totalConnections();
}
void BidderForwarderAsyncJobsService::runEveryHour() {
        while (true) {
                try {

                        entityToModuleStateStats->addStateModuleForEntity ("runEveryHour",
                                                                           "BidderForwarderAsyncJobsService",
                                                                           "ALL");

                        gicapods::Util::printMemoryUsage();
                } catch (std::exception const &e) {
                        gicapods::Util::showStackTrace();
                        //TODO : go red and show in dashboard that this is red
                        LOG(ERROR) << "error happening when runEveryHour  " << boost::diagnostic_information (e);
                }

                gicapods::Util::sleepViaBoost (_L_, 60 * 60);
        }

}

void BidderForwarderAsyncJobsService::runEvery10Seconds() {
        while (true) {
                try {


                } catch (std::exception &e) {
                        gicapods::Util::showStackTrace(&e);
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_runEvery10Seconds",
                                "BidderForwarderAsyncJobsService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }  catch (...) {
                        gicapods::Util::showStackTrace();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_runEvery10Seconds",
                                "BidderForwarderAsyncJobsService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );

                }

                gicapods::Util::sleepViaBoost (_L_, 10);
        }
}

void BidderForwarderAsyncJobsService::startBidderIfDead() {
        auto lastTimeBidderRanStr = FileUtil::readFileInString("/tmp/lastTimeBidderRan.txt");
        if (lastTimeBidderRanStr.empty() ||
            DateTimeUtil::getNowInSecond() - ConverterUtil::convertTo<long>(lastTimeBidderRanStr) > 120) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "BidderCrashed_startingNewOne",
                        "BidderForwarderAsyncJobsService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception
                        );

                int result = system("nohup /home/vagrant/workspace/Bidder/Bidder.out >/dev/null  2>&1 &");
                if (result != 0) {
                        LOG(ERROR)<<"failed to start bidder. result : "<< result;
                }
        }
}

void BidderForwarderAsyncJobsService::runEveryMinute() {
        while (true) {
                try {
                        // startBidderIfDead();
                        entityToModuleStateStats->addStateModuleForEntity ("runEveryMinute",
                                                                           "BidderForwarderAsyncJobsService",
                                                                           "ALL");
                        LOG_EVERY_N(INFO, 1) << "numberOfBidRequestRecievedPerMinute : "
                                             << numberOfBidRequestRecievedPerMinute->getValue();
                        LOG_EVERY_N(INFO, 1) << "numberOfBidRequestRecievedAvgPerSecond : "
                                             << numberOfBidRequestRecievedPerMinute->getValue() / 60;

                        printServerInfo();

                        numberOfBidRequestRecievedPerMinute->setValue(0);

                        originalBidderCallerModule->resetBidderFailures();
                } catch (std::exception &e) {
                        gicapods::Util::showStackTrace(&e);
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_EVERY_MINUTE",
                                "BidderForwarderAsyncJobsService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }  catch (...) {
                        gicapods::Util::showStackTrace();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_EVERY_MINUTE",
                                "BidderForwarderAsyncJobsService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );

                }

                gicapods::Util::sleepViaBoost (_L_, 60);
        }
}



std::string BidderForwarderAsyncJobsService::getName() {
        return "BidderForwarderAsyncJobsService";
}


void BidderForwarderAsyncJobsService::startAsyncThreads() {
        this->entityToModuleStateStatsPersistenceService->startThread();

        std::thread every10SecondsThread (&BidderForwarderAsyncJobsService::runEvery10Seconds, this);
        every10SecondsThread.detach();

        std::thread everyMinuteThread (&BidderForwarderAsyncJobsService::runEveryMinute, this);
        everyMinuteThread.detach();

        std::thread everyHourThread (&BidderForwarderAsyncJobsService::runEveryHour, this);
        everyHourThread.detach();

}
