/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef OriginalBidderCallerModule_H_
#define OriginalBidderCallerModule_H_

#include "Status.h"

class ForwarderContext;
class EntityToModuleStateStats;
class HttpUtilService;
class PocoSession;
#include <memory>
#include <vector>
#include <unordered_map>
#include <string>
#include "BidderForwarderModule.h"
#include "ConcurrentHashSet.h"
#include "ConfigService.h"
#include "Poco/Net/HTTPClientSession.h"
class OriginalBidderCallerModule : public BidderForwarderModule, public Object {

private:

std::vector<std::string> allBidderHosts;
std::shared_ptr<gicapods::ConcurrentHashMap<int, PocoSession > > indexToBidderHttpSessionMap;

std::string methodType;

public:

virtual std::string getName();
OriginalBidderCallerModule(
        std::vector<std::string> allBidderHosts,
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats);

virtual void process(std::shared_ptr<ForwarderContext> context);

void resetBidderFailures();
int getSessionBasedOnUserId(std::shared_ptr<ForwarderContext> context);

};

#endif
