#include "Status.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "ForwarderContext.h"

#include "EntityToModuleStateStats.h"
#include "ForwardContextBuilderModule.h"
#include "ConfigService.h"
#include "JsonUtil.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "ExceptionUtil.h"
#include "EntityToModuleStateStats.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Timestamp.h"
#include "Device.h"
#include "DeviceTypeConverter.h"
#include "OpenRtbVersion.h"
#include "ConcurrentHashMap.h"

ForwardContextBuilderModule::ForwardContextBuilderModule(
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats)
        : BidderForwarderModule(entityToModuleStateStats), Object(__FILE__) {
}


std::string ForwardContextBuilderModule::getName() {
        return "ForwardContextBuilderModule";
}


void ForwardContextBuilderModule::process(std::shared_ptr<ForwarderContext> context) {
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest =
                openRtbBidRequestParser->parseBidRequest(context->requestBody);
        auto deviceType = DeviceTypeConverter::convert
                                  (openRtbBidRequest->deviceOpenRtb->deviceType, OpenRtbVersion::V2_3);
        context->device = std::make_shared<Device> (openRtbBidRequest->user->buyeruid, deviceType);

}
