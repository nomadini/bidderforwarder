#include "BidderForwarderRequestHandlerFactory.h"
#include "StringUtil.h"
#include "TempUtil.h"
#include "BidRequestForwarder.h"
#include "GUtil.h"
#include "ConverterUtil.h"
#include "ForwarderContext.h"
#include <boost/foreach.hpp>
#include "ConfigService.h"
#include "TargetGroup.h"
#include "BidRequestForwarderPipelineProcessor.h"
#include "HttpUtil.h"
#include "BidRequestForwarderPipelineProcessor.h"

BidderForwarderRequestHandlerFactory::BidderForwarderRequestHandlerFactory(
        BidRequestForwarderPipelineProcessor* bidRequestForwarderPipelineProcessor,
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService) {

        this->bidRequestForwarderPipelineProcessor = bidRequestForwarderPipelineProcessor;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->configService = configService;
}

Poco::Net::HTTPRequestHandler
*BidderForwarderRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        try {
                LOG_EVERY_N(INFO, 10000) << google::COUNTER <<"nth request uri to process : " << request.getURI ();

                if (StringUtil::containsCaseInSensitive (request.getURI (), "/bid/google/openrtb2.3")) {
                        auto bidRequestForwarder =  createBidRequestForwarder(
                                "/bid/google/openrtb2.3",
                                OpenRtbVersion::V2_3);

                        return bidRequestForwarder;
                }

        } catch (...) {
                LOG(ERROR) <<"unknow exception happened";
        }
        return nullptr;
        // return new UnknownRequestHandler (entityToModuleStateStats);
}

BidRequestForwarder* BidderForwarderRequestHandlerFactory::createBidRequestForwarder(
        std::string uriPath,
        OpenRtbVersion openRtbVersion
        ) {
        auto context  = std::make_shared<ForwarderContext>();
        context->exchangeName = "google";
        context->openRtbVersion = openRtbVersion;
        context->servletPathToForwardTo = uriPath;
        auto bidRequestForwarder = new BidRequestForwarder (
                context,
                bidRequestForwarderPipelineProcessor,
                entityToModuleStateStats);
        bidRequestForwarder->numberOfBidRequestRecievedPerMinute =
                numberOfBidRequestRecievedPerMinute;
        return bidRequestForwarder;
}
