#include "GUtil.h"
#include "StringUtil.h"
#include "BidderForwarderAppStarter.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "ConverterUtil.h"
#include <thread>
#include "ConcurrentHashMap.h"
#include "FeatureDeviceHistory.h"
#include "BidderForwarderRequestHandlerFactory.h"
#include "CassandraDriverInterface.h"
#include "ProgramOptionParser.h"
#include "BidderForwarder.h"
#include "ForwarderModuleContainer.h"
#include "BidRequestForwarderPipelineProcessor.h"
#include "BidderForwarderAsyncJobsService.h"
#include "OriginalBidderCallerModule.h"
#include "ConcurrentHashMap.h"

void BidderForwarderAppStarter::processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory) {


        //The default_value will be used when the option is not specified at all. The implicit_value will be used when the option is specific without a value. If a value is specified, it will override the default and implicit.
        namespace po = boost::program_options;
        boost::program_options::options_description desc("Allowed options");
        desc.add_options()
        // First parameter describes option name/short name
        // The second is parameter to option
        // The third is description
                ("help,h", "print usage message")
                ("appname,a",
                po::value(&appName)->default_value("BidderForwarder"), "app name")
                ("property,p", boost::program_options::value(&propertyFileName)->
                default_value("bidderforwarder.properties"), "bidder forwarder property file name")
                ("port,t", boost::program_options::value(&port)->default_value(1), "bidder forwarder port");
        ("log_dir,l", boost::program_options::value(&logDirectory)->default_value("/var/log/"), "log directory location");

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
                std::cout << desc << "\n";
                return;
        }


        std::cout << "property = " << vm["property"].as<std::string>() << "\n";
        std::cout << "port = " << vm["port"].as<unsigned short>() << "\n";
        std::cout << "appname = " << vm["appname"].as<std::string>() << "\n";

        //THE REAL ASSIGNMENT HAPPENS HERE !!
        appName = vm["appname"].as<std::string>();
        propertyFileName = vm["property"].as<std::string>();
        port = vm["port"].as<unsigned short>();
}

void BidderForwarderAppStarter::startTheApp(int argc, char* argv[]) {


        std::string appName = "BidderForwarder";
        std::string logDirectory = "";
        std::string propertyFileName = "bidderforwarder.properties";
        unsigned short bidderForwarderPort = 1;

        processArguments(argc,
                         argv,
                         propertyFileName,
                         bidderForwarderPort,
                         appName,
                         logDirectory);

        TempUtil::configureLogging (appName, argv);
        LOG(ERROR) << appName <<" starting with properties "<< propertyFileName <<
                std::endl<<" original arguments : argc "<< argc << " argv : " <<  *argv;

        auto numberOfBidRequestRecievedPerMinute =
                std::make_shared<gicapods::AtomicLong>();

        auto moduleContainer = std::make_unique<ForwarderModuleContainer>();
        moduleContainer->propertyFileName = propertyFileName;
        moduleContainer->commonPropertyFileName = "common.properties";
        moduleContainer->initializeModules();

        auto numberOfExceptionsInWritingEvents = std::make_shared<gicapods::AtomicLong>();
        auto numberOfBidsForPixelMatchingPurposePerHour = std::make_shared<gicapods::AtomicLong>();
        auto numberOfExceptionsInWritingMessagesInKafka = std::make_shared<gicapods::AtomicLong>();
        auto queueOfContexts = std::make_shared<tbb::concurrent_queue<std::shared_ptr<ForwarderContext> > >();
        tbb::concurrent_queue<std::shared_ptr<ForwarderContext> >::size_type size = 10000;


        std::string allOriginalBiddersInOneString;
        moduleContainer->configService->get("bidderUrlsToForwardTo", allOriginalBiddersInOneString);
        auto allBidderHosts = StringUtil::split(allOriginalBiddersInOneString, ",");
        assertAndThrow(!allBidderHosts.empty());

        auto originalBidderCallerModule = std::make_unique<OriginalBidderCallerModule> (
                allBidderHosts,
                moduleContainer->configService.get(),
                moduleContainer->entityToModuleStateStats.get()
                );

        auto bidderForwarderAsyncJobsService = std::make_shared<BidderForwarderAsyncJobsService>();
        bidderForwarderAsyncJobsService->configService = moduleContainer->configService.get();
        bidderForwarderAsyncJobsService->entityToModuleStateStats = moduleContainer->entityToModuleStateStats.get();
        bidderForwarderAsyncJobsService->entityToModuleStateStatsPersistenceService = moduleContainer->entityToModuleStateStatsPersistenceService.get();
        bidderForwarderAsyncJobsService->originalBidderCallerModule = originalBidderCallerModule.get();
        bidderForwarderAsyncJobsService->numberOfBidRequestRecievedPerMinute = numberOfBidRequestRecievedPerMinute;





        auto bidRequestForwarderPipelineProcessor =
                std::make_shared<BidRequestForwarderPipelineProcessor>();

        bidRequestForwarderPipelineProcessor->preBidModules.push_back(
                (BidderForwarderModule*) moduleContainer->forwardContextBuilderModule.get());
        bidRequestForwarderPipelineProcessor->preBidModules.push_back(originalBidderCallerModule.get());
        bidRequestForwarderPipelineProcessor->entityToModuleStateStats =
                moduleContainer->entityToModuleStateStats.get();

        auto bidderForwarderRequestHandlerFactory =
                std::make_shared<BidderForwarderRequestHandlerFactory>(
                        bidRequestForwarderPipelineProcessor.get(),
                        moduleContainer->entityToModuleStateStats.get(),
                        moduleContainer->configService.get()

                        );
        bidderForwarderRequestHandlerFactory->numberOfBidRequestRecievedPerMinute = numberOfBidRequestRecievedPerMinute;
        auto bidderForwarder = std::make_shared<BidderForwarder>();

        bidderForwarder->configService = moduleContainer->configService.get();
        bidderForwarder->propertyFileName = propertyFileName;
        bidderForwarder->entityToModuleStateStats = moduleContainer->entityToModuleStateStats.get();
        bidderForwarder->requestHandlerFactory = bidderForwarderRequestHandlerFactory.get();

        bidderForwarder->init();
        bidderForwarderAsyncJobsService->httpServer = bidderForwarder->httpServer;
        bidderForwarderAsyncJobsService->startAsyncThreads();

        argc = 1; //we set the number of options to 1
        //because we don't want bidder to process options
        bidderForwarder->run(argc, argv);
}
