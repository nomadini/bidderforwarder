#ifndef BidderForwarderModule_H
#define BidderForwarderModule_H


#include "Status.h" //this is needed in every module
class ForwarderContext;
#include <memory>
#include <string>
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
class BidderForwarderModule;


class BidderForwarderModule : public Object {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
BidderForwarderModule(EntityToModuleStateStats* entityToModuleStateStats);

virtual void beforeProcessing(std::shared_ptr<ForwarderContext> context);
virtual std::string getName();

virtual void process(std::shared_ptr<ForwarderContext> context)=0;

virtual void afterProcessing(std::shared_ptr<ForwarderContext> context);

virtual ~BidderForwarderModule();
};

#endif
