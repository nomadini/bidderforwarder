#ifndef BidRequestForwarderPipelineProcessor_H
#define BidRequestForwarderPipelineProcessor_H

class ForwarderContext;
#include <memory>
#include <string>
#include "AtomicLong.h"
#include "BidderForwarderModule.h"
#include "Poco/Timestamp.h"
class EntityToModuleStateStats;
namespace gicapods {class ConfigService; }

class EntityToModuleStateStats;

class BidRequestForwarderPipelineProcessor {
public:

std::vector<BidderForwarderModule*> preBidModules;
EntityToModuleStateStats* entityToModuleStateStats;
BidRequestForwarderPipelineProcessor();
void runModule(
        BidderForwarderModule* module,
        std::shared_ptr<ForwarderContext> context);
void setTheModuleList();
void addToPreBidModuleList(BidderForwarderModule* module);
void runPostBidModules(std::shared_ptr<ForwarderContext> opt);
virtual void process(std::shared_ptr<ForwarderContext> opt);

std::string getName();

virtual ~BidRequestForwarderPipelineProcessor();
};

#endif
